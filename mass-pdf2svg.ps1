param($scriptPath)

Get-ChildItem -Filter "*.pdf" | ForEach-Object {
    & $scriptPath $_.Name ($_.BaseName + ".svg")}
