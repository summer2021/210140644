#!/usr/bin/env pwsh
# Converts PDF to SVG with Inkscape.

param (
    [Parameter(Mandatory = $true,
        HelpMessage = 'Enter the input filename')]
    [string] $InputPath,

    [Parameter(Mandatory = $true,
        HelpMessage = 'Enter the output filename')]
    [string] $OutputPath
)

inkscape --pdf-poppler -l -o $OutputPath $InputPath
